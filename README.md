# MybatisPlus 整合 Redis 缓存的CRUD开发脚架

#### 项目介绍
MybatisPlus 整合 Redis 缓存的CRUD开发脚架, 在我们搭建项目, 整合MybatisPlus后, 如果要提升查询性能, 一般都会采用Redis作为缓存, 但是采用网上一些开源的Redis注解方式框架整合, 发现效果并不理想, 不能灵活地控制缓存粒度和保持MySQL与Redis的事务同步, 所以自己整合了这套crud开发脚架, 并且开源,避免重复造轮子,有需要的可以拿去参考和使用;

#### 注意事项

1. 由于有些操作采用了Redis的事务模板进行操作, 如果你做的是微服务, 并且使用第三方分布式事务框架是LCN,则不适用这套crud,原因
是LCN进行分布式事务协调时,对MySQL连接做了假提交,当分布式事务成功后,才真正提交事务操作到MySQL数据库,而当分布式事务失败后则
不提交事务操作到MySQL,但是RedisTranTemplate当LCN做出假提交后,都会提交事务操作到Redis数据库,所以当LCN分布式事务失
败后,就会出现Redis的事务操作却不能同步回滚的情况,导致缓存中出现脏数据;


2.现在的查询方式已足以应对日常开发遇到的问题,在生成代码时,已限制不采用ser user = new User(); user.insert()这种方式进行操作
 [ 原因 : 这种方式本项目还没整合Redis操作,所以不能使用该方式进行操作,否则会造成缓存不一致的情况! ]";

3.了解Redis的人,都知道它的事务其实并不是完整的事务,无法保证一次性提交的事务中的每一个命令都能执行成功,但是绝大多数情况下都是可以全部正确执行的, 如果不使用事务与MySQL整合,后果是更加严重的,例如在注册时插入用户表时发生异常,MySQL的数据已经回滚,但Redis中却不会滚,成功提交了, 脏数据必然会产生,导致缓存中已经有一个user对象;


#### 软件架构
1.Mybatis-plus : 采用能快速进行 CRUD 操作的 Mybatis-plus, 当前版本 3.0.5 , 官方网址 http://mp.baomidou.com/;

2.Redis序列化方式 : 采用高性能省内存空间的Google开源的序列化方式 Protostuff, 序列化性能对比 https://www.cnblogs.com/lonelywolfmoutain/p/5563985.html;

	耗时对比:
![Image text](http://pd6aqb81h.bkt.clouddn.com/%E5%BA%8F%E5%88%97%E5%8C%96%E8%80%97%E6%97%B6%E5%AF%B9%E6%AF%94.jpg)

	空间占用对比:
![Image text](http://pd6aqb81h.bkt.clouddn.com/%E5%BA%8F%E5%88%97%E5%8C%96%E8%80%97%E7%A9%BA%E9%97%B4%E5%AF%B9%E6%AF%94.jpg)

感谢 caodongfang126 的博客文章分享的 ProtostuffUtils



	测试分页查询
压测工具 Gatling
25秒内启动完线程300个, 持续压测86秒
![Image text](http://pd6aqb81h.bkt.clouddn.com/%E6%B5%8B%E8%AF%95%E5%88%86%E9%A1%B5%E6%9F%A5%E8%AF%A2.png)
每秒qps大概7000多

![Image text](http://pd6aqb81h.bkt.clouddn.com/%E5%8E%8B%E5%8A%9B%E6%B5%8B%E8%AF%95%E5%88%86%E9%A1%B5%E6%9F%A5%E8%AF%A22.gif)
99%的请求时间低于50毫秒

#### 安装教程
需要安装 Redis, MySQL, IDEA, Maven等

#### 使用说明

1. 修改数据库连接信息: application.yml 的 MySQL连接信息, config\redis.properties的redis连接信息;
2. 执行脚本 \db\crud.sql 初始化数据库;
3. 执行测试用例,参考 UserServiceTest;
4. 整合方式, MybatisPlus不使用Redis,继承ServiceImpl; MybatisPlus使用Redis 继承 ServiceWithRedisImpl即可;

#### 开发计划
1.后期新增配套的代码生成器;    已添加
2.完善使用例子
